import { Component, OnInit } from '@angular/core';
import { IssNowService } from './shared/services/iss-now.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ IssNowService ]
})
export class AppComponent implements OnInit {

  public position;
  public people;

  constructor( private issNowService: IssNowService) {} 

  ngOnInit() {
    this.getDataIssNow();
    this.getPersons();
    setInterval(() => {
      this.getDataIssNow(); }, 1000);
  } 

  // Obtener posicion
  getDataIssNow() {
    this.issNowService.getIssNow().subscribe(
      data => {
        if (data.message === "success") {
          this.position = {
            timestamp: data.timestamp,
            iss_position: [ this.convertStringToNumber(data.iss_position.latitude), this.convertStringToNumber(data.iss_position.longitude) ]
          } 
        } else {
          console.log("Ha habido un error");
        }
      },
      error => {
        console.log("Ha habido un error");
      }
    );
  }

  // Obtener número de Personas en el Espacio
  getPersons() {
    this.issNowService.getPersons().subscribe(
      data => {
        if (data.message === "success") {
          this.people = data.people; 
        } else {
          console.log("Ha habido un error");
        }
      },
      error => {
        console.log("Ha habido un error");
      }
    );
  }

  // Pasar latitud y longitud a numero
  public convertStringToNumber(value: string): number {
    return parseFloat(value);
  }
  
}
