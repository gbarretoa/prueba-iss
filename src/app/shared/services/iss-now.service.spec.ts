import { TestBed, inject } from '@angular/core/testing';

import { IssNowService } from './iss-now.service';

describe('IssNowService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IssNowService]
    });
  });

  it('should be created', inject([IssNowService], (service: IssNowService) => {
    expect(service).toBeTruthy();
  }));
});
