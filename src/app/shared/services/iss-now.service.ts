import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class IssNowService {

  constructor(private http: HttpClient) { }

   // Obtener Posición Actual
   getIssNow(): Observable<any> {
    return this.http.get("http://api.open-notify.org/iss-now.json", {} );
  }
  
  // Obtener Personas
  getPersons(): Observable<any> {
    return this.http.get("http://api.open-notify.org/astros.json", {} );
  }
}
